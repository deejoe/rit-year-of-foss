# Year of FOSS

This repo contains a list of 
[RIT](https://www.rit.edu) 
[FOSS Minor](https://www.rit.edu/programs/minors/free-and-open-source-software-and-free-culture) 
[courses](https://www.rit.edu/infocenter/) for the 
[2018-2109 academic year](https://www.rit.edu/calendar/future-chart.html).

The charts were created in [emacs org-mode](http://orgmode.org/) and converted into other formats
using [pandoc](http://pandoc.org/)

```
 pandoc -o courses-2018-2019.html courses-2018-2019.org 

 pandoc -o courses-2018-2019.md courses-2018-2019.org
```

Unless directly contributing HTML boilerplate (styling, headers, footers, etc)
please edit the Markdown (.md) or org-mode (.org) versions as they are more
cleanly versionable.


 