A Year of FOSS: 2018-2019
=========================

### Fall 2018

  ---------- ------------------------------------ ------ ------- -------------------
  number     title                                term   days    time
  IGME 583   Legal and Business Aspects of FOSS   Fall   Tu/Th   12:30PM - 1:45PM
  ISTE 252   Foundations of Mobile Design         Fall   MWF     1:00PM - 1:50PM
  CSCI 715   Applications of Virtual Reality      Fall   Tu/Th   11:00AM - 12:15PM
  ENGL 351   Language Technology                  Fall   MWF     12:00PM - 12:50PM
  ENGL 361   Technical Writing                    Fall   Tu/Th   2:00PM - 3:15PM
  ENGL 450   Free & Open Source Culture           Fall   MWF     10:00AM - 10:50AM
  number     title                                term   days    time
  ---------- ------------------------------------ ------ ------- -------------------

### Spring 2019

  ---------- ---------------------------------------------------- -------- ------- ------------------
  number     title                                                term     days    time
  IGME 582   Humanitarian Free/Open Source Software Development   Spring   MWF     9:00AM - 9:50AM
  IGME 585   Project in FOSS Development                          Spring   Tu/Th   8:00AM - 9:15AM
  ENGL 361   Technical Writing                                    Spring   Tu/Th   9:30AM - 10:45AM
  ENGL 215   Text & Code                                          Spring   Wed     5:00PM - 7:50PM
  ENGL 351   Language Technology                                  Spring   Tu/Th   9:30AM - 10:45AM
  number     title                                                term     days    time
  ---------- ---------------------------------------------------- -------- ------- ------------------


